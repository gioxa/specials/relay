//
//  connect_socks.c
//  portsocksmap
//
//  Created by Danny Goossen on 24/4/2020.
//  Copyright © 2020 Danny Goossen. All rights reserved.
//

#include "connect_socks.h"

#include "socks_conn.h"
#include "network.h"
#include "compat.h"
#include "error.h"

sockfd_t connect_host(int connect_timeout,const char*host, u_int16_t port);
int connect_socks_proxy(sockfd_t fd, const char * host,u_int16_t port);

sockfd_t connect_socks5(const char*proxy_host, u_int16_t proxy_port,const char*host, u_int16_t port,int connect_timeout)
{
	sockfd_t sfd=connect_host(connect_timeout,proxy_host,proxy_port);
	if (sfd==INVALID_SOCKET) return sfd;
	int ret=connect_socks_proxy(sfd, host, port);
	if (ret==-1)
	{
		close(sfd);
		sfd=INVALID_SOCKET;
	}
	return sfd;
}

int connect_socks_proxy(sockfd_t fd, const char * host,u_int16_t port)
{
    char * message=calloc(257,1);
    ssize_t read_len=0;
    u_int16_t portn=htons(port);
    u_int16_t sendlen=3;
    ssize_t sen_len;
	socket_blocking(fd);
    sprintf(message, "\5\1");
    sen_len=send(fd, message, 3,0);
    if (sen_len==SOCKET_ERROR || sen_len!=3)
    {
	if (sen_len==SOCKET_ERROR)
           syslog_sock_error("CSP","send_socks helo");
        else
	  syslog(LOG_NOTICE, "fd %d problem sending socks helo len=%zd of %d",fd,sen_len,3);
	return -1;
    }
    read_len=recv(fd, message,2,0);
    if (read_len==2 && message[0]==5 && message[1]==0)
    {
	syslog(LOG_NOTICE, " helo received ");
        message[0]=5;
        message[1]=1;
        message[2]=0;
        message[3]=3;
        message[4]=(u_int8_t) strlen(host);
        sprintf(&message[5],"%s",host);
        memcpy((&message[5+(u_int8_t) strlen(host)]), & portn, 2);
        sendlen=7+(u_int8_t) strlen(host);
        //hex_dump_msg(message, sendlen ," connect string to sent");
        sen_len=send(fd, message, sendlen,0);
        if (sen_len==SOCKET_ERROR || sen_len!=sendlen)
	{
		if (sen_len==SOCKET_ERROR) syslog_sock_error("CSP","send_socks connect");
                else
		syslog(LOG_NOTICE, " problem sending socks connect len=%zd of %d",sen_len,sendlen);
	   return -1;
	}
	read_len=recv(fd, message,10,0);
        if ((read_len==10)&& message[0]==5 && message[1]==0)
        {
	    	syslog(LOG_NOTICE, "connected to socks proxy");
            return 1;
        }
        else
        {
	  if (read_len==SOCKET_ERROR)
	    syslog_sock_error("CSP","problem rec socks connect reply");
          else
	    syslog(LOG_NOTICE, "problem rec socks connect reply, len=%zd of 10, socksv=%c,ok=%c ",read_len,message[0],message[1]);
            return -1;
        }
    }
    else if (read_len==SOCKET_ERROR)
	   syslog_sock_error("CSP","recive_socks helo reply");
    else
	   syslog(LOG_NOTICE, " problem rec sock helo reply, len=%zd of %d",read_len,2);
    return -1;
}



sockfd_t connect_host(int connect_timeout,const char*host, u_int16_t port)
{
	sockfd_t fd=INVALID_SOCKET;
	socklen_t len;
	
	fd_set fdset,fdsetmaster,readfds,masterreadfds;
	struct timeval tv;
	int nready,maxfd=0;
	
	struct sockaddr_storage temp_adress;
	struct sockaddr * sock_addr=(struct sockaddr*)&temp_adress;
	socklen_t  sock_addr_len=NET_ADDR_SIZE;
	u_int32_t  client_addr=inet_addr(host);

	sock_addr->sa_family=AF_INET;
	memcpy(&((struct sockaddr_in *)sock_addr)->sin_addr,&client_addr, 4);
	
	((struct sockaddr_in*)sock_addr)->sin_port=htons( port );
	
	sock_addr_len=16;
	fd = socket((sock_addr)->sa_family,SOCK_STREAM ,IPPROTO_TCP);


	if (fd==INVALID_SOCKET ) return (fd);
	if (fd==0) info ("what the fuck, why after connect this is 0\n");

	#ifdef SO_NOSIGPIPE
	int set=1;
	setsockopt(fd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
	#endif
	socket_nonblocking(fd);

	FD_ZERO(&fdsetmaster);
	FD_ZERO(&fdset);
	FD_ZERO(&readfds);
	FD_ZERO(&masterreadfds);
	FD_SET(fd, &fdsetmaster);
	maxfd=fd +1;

	connect(fd, sock_addr, sock_addr_len);
	do
	{
		tv.tv_sec = 0;
		tv.tv_sec +=connect_timeout;
		/* timeout */
		tv.tv_usec = 0;
		memcpy(&fdset, &fdsetmaster, sizeof(fdsetmaster));
		memcpy(&readfds, &masterreadfds, sizeof(masterreadfds));
		nready =select(maxfd, &readfds, &fdset, NULL, &tv);
		if (nready == 0) { shutdown(fd,SHUT_RDWR);close_socket(fd); fd=INVALID_SOCKET; } // timeout
		if (nready > 0)
		{
			if(FD_ISSET(fd, &fdset))
			{
				int val;
				len = sizeof(val);
				if(getsockopt(fd,SOL_SOCKET,SO_ERROR,&val,&len)<0){
					shutdown(fd,SHUT_RDWR); close (fd); fd=INVALID_SOCKET;
							return fd;
				}
				if(val!=0){
					shutdown(fd,SHUT_RDWR); close (fd); fd=INVALID_SOCKET;
							return fd;
				}
				return (fd); // can write and no socket errors, thus connected
			}
		}
	} while (nready < 0 && errno == SCK_EINTR);
	// close(fd); // job of the caller?
	if (fd!=INVALID_SOCKET) {shutdown(fd,SHUT_RDWR); close (fd); fd=INVALID_SOCKET;}


	return fd;
}

