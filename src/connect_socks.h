//
//  connect_socks.h
//  portsocksmap
//
//  Created by Danny Goossen on 24/4/2020.
//  Copyright © 2020 Danny Goossen. All rights reserved.
//

#ifndef connect_socks_h
#define connect_socks_h

#include "network.h"


sockfd_t connect_socks5(const char*proxy_host, u_int16_t proxy_port,const char*host, u_int16_t port,int connect_timeout);

sockfd_t connect_host(int connect_timeout,const char*host, u_int16_t port);

#endif /* connect_socks_h */
