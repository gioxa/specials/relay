//
//  config.c
//  livestreamer
//
//  Created by Danny Goossen on 29/9/16.
//  Copyright (c) 2016 Danny Goossen. All rights reserved.
//


#include "globals.h"
#include "config.h"
#include <getopt.h>



static const char * PID_FILE="/tmp/relay.pid";

static const char* 		HOST_A	="127.0.0.1";
static const char* 		HOST_B  ="127.0.0.1";
static const u_int16_t 	PORT_A	= 30005;
static const u_int16_t 	PORT_B	= 30004;


//static const char* 		SOCKS_IP	= "192.168.2.153";
static const char* 		SOCKS_IP	= NULL;
static const u_int16_t 	SOCKS_PORT	= 1080;
static const u_char     IPV4ONLY	= 1;            /* the default IPV6 setting                */
static const u_char     VERBOSE_YN	= 1;            /* the default verbosity setting                */

#ifdef TEST
static const u_int8_t	DAEMONIZE_YN= 1;
#else
static const u_int8_t	DAEMONIZE_YN= 0;
#endif

#ifndef GITVERSION
#define GITVERSION "no_git"
#endif



/*------------------------------------------------------------------------
 * int process_options(int argc, char **argv, tb_parameter_t *parameter)
 *
 * Process the program argument options and set the parameters accordingly
 *------------------------------------------------------------------------*/
int process_options(int argc, char **argv, struct parameter_t *parameter)
{
	
	struct option long_options[] = {
		{ "verbose",    0, NULL, 'v' },
		{ "quiet",      0, NULL, 'q' },
		{ "port_a",     1, NULL, 'p' },
		{ "host_a",     1, NULL, 'a' },
		{ "proxyport",	1, NULL, 'y' },
		{ "proxy",      1, NULL, 'x' },
		{ "host_b",     1, NULL, 'b' },
		{ "port_b",     1, NULL, 'P' },
		{ "daemon",     0, NULL, 'd' },
		{ "exec",       0, NULL, 'e' },
		{ "config",     1, NULL, 'c' },
		{ "pid",        1, NULL, 'i' },
		{ "help",       0, NULL, 'j' },
		{ "version",    0, NULL, 'g' },
		{ "v4only",     0, NULL, '4' },
		{ "v6",         0, NULL, '6' },
		{ NULL,         0, NULL, 0 } };
	
	int           which;
	optind=1;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long(argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameter->quiet)
					{
						printf("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameter->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameter->verbose)
					{
						printf("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					parameter->quiet = 1;
				}
					break;

					/* --v4         : disable IPv6 mode */
				case '4':
					
					if (parameter->ipv6)
					{
						printf("Invalid option, choose v4only or v6\n");
						return -1;
					}
					else
						parameter->ipv4only = 1;
					break;
				case '6':
					
					if (parameter->ipv4only)
					{
						printf("Invalid option, choose v4only or v6\n");
						return -1;
					}
					else
						parameter->ipv6 = 1;
					break;
					/* --port=i     : port number of the server */
				case 'p':
				{
					parameter->port_a   = atoi(optarg);
					printf(" port_a: %d\n",parameter->port_a);
					
				}
				break;
				case 'P':
				{
					parameter->port_b   = atoi(optarg);
					printf(" port_b: %d\n",parameter->port_b);
					
				}
				break;
				case 'a': {
					
					parameter->host_a =malloc(strlen(optarg)+1);
					sprintf(parameter->host_a,"%s",optarg);
					printf( " host_a %s \n",parameter->host_a );
				}
				break;	
				case 'b': {
					
					parameter->host_b =malloc(strlen(optarg)+1);
					sprintf(parameter->host_b,"%s",optarg);
					printf( " host_b %s \n",parameter->host_b );
				}
				break;	
				case 'x':
					parameter->proxy =malloc(strlen(optarg)+1);
					sprintf(parameter->proxy,"%s",optarg);
					printf( " Proxy %s \n",parameter->proxy);
					break;
					
				case 'y':
					parameter->proxyport   = (u_int16_t)atoi(optarg);
					printf( " Proxyport %d \n",parameter->proxyport );
					break;
					
				case 'c': {
					parameter->config_file=malloc(strlen(optarg)+1);
					sprintf(parameter->config_file,"%s",optarg);
					// printf( " conf-file > %s ",parameter->config_file );
				}
					break;
				case 'i': {
					parameter-> pidfile=malloc(strlen(optarg)+1);
					sprintf(parameter->pidfile,"%s",optarg);
					printf( " pid-file > %s\n ",parameter->pidfile );
				}
					break;
					
				case 'd':
				{
					if (parameter->exec)
					{
						printf("Invalid option, choose daemon or exec\n");
						return -1;
					}
					else
					{
						parameter->daemon = 1;
					}
				}
					break;
				case 'e':
				{
					if (parameter->daemon)
					{
						printf("Invalid option, choose daemon or exec\n");
						return -1;
					}
					else
					{
						parameter->exec = 1;
					}
				}
					break;
					
				case 'g':
				{
					
					printf("%s\n",GITVERSION);
					exit(0);
				}
				case 'j': {
					printf("\nrelay Git Version: %s\n \n",GITVERSION);
					goto show_usage;
				}
					
					
					/* otherwise    : display usage information */
				default:
					;
				show_usage:
					fprintf(stderr, "Usage: relay [--verbose] [--v6] [host_b=192.168.1.1] [--port_a=n]\n");
					fprintf(stderr, "               [--daemon] [--config=/etc/streamer.conf] .... \n\n");
					fprintf(stderr, "version      : Shows the version and usage\n");
					fprintf(stderr, "help         : Shows the version and usage\n");
					fprintf(stderr, "verbose      : turns on verbose output mode\n");
					fprintf(stderr, "quiet        : quiet down output\n");
					fprintf(stderr, "v4only       : enable only IPV4\n");
					fprintf(stderr, "v6           : enable IPV6\n");
					
					fprintf(stderr, "daemon       : runs tblastd in background as a service\n");
					fprintf(stderr, "exec         : disable daemon\n");
					fprintf(stderr, "port_a       : specifies the port of the A host\n");
					fprintf(stderr, "host_a       : specifies the ipv4 adress of the A host\n");
					fprintf(stderr, "port_b       : specifies the port of the B host\n");
					fprintf(stderr, "host_b       : specifies the ipv4 adress of the B host\n");					
//					fprintf(stderr, "socks_port   : specifies the port of the socks server!\n");
//					fprintf(stderr, "proxy        : specifies the the socksproxy ip adress!\n");					
//					fprintf(stderr, "secret       : specifies the shared secret for the client and server\n");
					fprintf(stderr, "config       : specifies the config file to be used\n");
					fprintf(stderr, "               !! use '--config=none' to skip reading a config file all together \n");
					fprintf(stderr, "pid          : specifies the pid  file to be used\n");
					fprintf(stderr, "\n");
					
					fprintf(stderr, "Defaults: verbose    = %d\n",   VERBOSE_YN);
					fprintf(stderr, "          v4only     = %d\n",   IPV4ONLY );
					fprintf(stderr, "          daemon     = %d\n",   DAEMONIZE_YN);
					fprintf(stderr, "          port_a     = %d\n",   PORT_A);
					fprintf(stderr, "          port_b     = %d\n",   PORT_B );
					fprintf(stderr, "          host_a     = %s\n",   HOST_A);
					fprintf(stderr, "          host_b     = %s\n",   HOST_B);
					fprintf(stderr, "          socks_port = %d\n",   SOCKS_PORT );
					fprintf(stderr, "          socks_ip   = %s\n",	 SOCKS_IP );
					fprintf(stderr, "          config     = no_config\n");
					fprintf(stderr, "          pid        = %s\n",  PID_FILE);
					fprintf(stderr, "\n");
					exit(0);
			}
		}
	}
	// if NULL then no option was given and we default define the dir where tblastd was started !!!!!!
	//value = getenv(name);
	return 0;
}

/*------------------------------------------------------------------------
 * void rset_missing_parms(tb_parameter_t *parameter)
 *
 * set the parameters to default if not in commandline or config
 *------------------------------------------------------------------------*/
void set_missing_parms(struct parameter_t *tparameter)
{
	if (tparameter->pidfile == NULL)
	{
		tparameter->pidfile=calloc(strlen(PID_FILE)+1,1);
		sprintf(tparameter->pidfile,"%s",PID_FILE);
	}
	if (! tparameter->verbose && !tparameter->quiet) tparameter->verbose = VERBOSE_YN;
	if (! tparameter->daemon && ! tparameter->exec) tparameter->daemon = DAEMONIZE_YN;
	if (! tparameter->ipv6 && !tparameter->ipv4only) tparameter->ipv6 = ! IPV4ONLY ;
	
	if (! tparameter->host_a)
	{
		tparameter->host_a=(char*)calloc(strlen(HOST_A)+1,1);
			sprintf(tparameter->host_a,"%s",HOST_A);
	}
	if (! tparameter->host_b)
	{
		tparameter->host_b=(char*)calloc(strlen(HOST_B)+1,1);
			sprintf(tparameter->host_b,"%s",HOST_B);
	}
	if (! tparameter->port_a) tparameter->port_a = PORT_A ;
	if (! tparameter->port_b) tparameter->port_b = PORT_B ;
	
	if (! tparameter->proxy && SOCKS_IP)
	{
		tparameter->proxy=(char*)calloc(strlen(SOCKS_IP)+1,1);
		sprintf(tparameter->proxy,"%s",SOCKS_IP);
	}
	if (! tparameter->proxyport) tparameter->proxyport = SOCKS_PORT;
//	if (! parameter->secret) parameter-> = ;
	//printf("bind_address:%s: adrr %p",tparameter->bind_adress,&(tparameter->bind_adress));
	return;
	
	
}


/*------------------------------------------------------------------------
 * void readConfigFile(tb_parameter_t *parameter)
 *
 * Read the config file, and set's the parametrs accordingly
 *------------------------------------------------------------------------*/
#define MAX_LINE_LEN 1024
#define SBUF_SIZE 100
int readConfigFile(struct parameter_t *parameter)
{
	FILE* config_fp ;
	char line[MAX_LINE_LEN + 1 ];
	char* val ;
	char* par;
	
	config_fp = fopen(parameter->config_file, "r");
	if (!config_fp)
	{
		fprintf(stderr,"Could not open config file\n");
		return -1;
	}
	while( fgets( line, MAX_LINE_LEN, config_fp ) != NULL )
	{
		par = strtok( line, "\t =\n\r" ) ;
		
		if( par != NULL && par[0] != '#' )
		{
			val = strtok( NULL, "\t =\n\r" ) ;
			if (val != NULL)
			{
				if (strcmp(par,"port_a")==0)
				{
					if (parameter->port_a)
					{
						printf( " Port a set at cmd line, ignoring config\n" );
					}
					parameter->port_a  = (u_int16_t) atoi(val);
				}
				else if (strcmp(par,"port_b")==0)
				{
					if (parameter->port_b)
					{
						printf( " port_b set at cmd line, ignoring config\n" );
					}
					parameter->port_b  = (u_int16_t) atoi(val);
				}
				else if (strcmp(par,"host_a")==0)
				{
					if (parameter->host_a)
					{
						printf( " host_a set at cmd line, ignoring config\n" );
					}
					else
					{
						parameter->host_a =malloc(strlen(val)+1);
						sprintf(parameter->host_b,"%s",val);
						printf( " host_a %s \n",parameter->host_a );
					}
				}
				else if (strcmp(par,"host_b")==0)
				{
					if (parameter->host_b)
					{
						printf( " host_b set at cmd line, ignoring config\n" );
					}
					else
					{
						parameter->host_b =malloc(strlen(val)+1);
						sprintf(parameter->host_b,"%s",val);
						printf( " host_b %s \n",parameter->host_b );
					}
				}
				else if (strcmp(par,"proxy")==0)
				{
					if (parameter->proxy)
					{
						printf( " Proxy set at cmd line, ignoring config\n" );
					}
					else
					{
						parameter->proxy =malloc(strlen(val)+1);
						sprintf(parameter->proxy,"%s",val);
						printf( " Host Name %s \n",parameter->proxy );
					}
					
				}
				else if (strcmp(par,"proxyport")==0)
				{
					if (parameter->proxyport)
					{
						printf( " Proxy-port set at cmd line, ignoring config\n" );
					}
					else
					{
						parameter->proxyport  = (u_int16_t)atoi(val);
					}
				}
				else if (strcmp(par,"pid")==0)
				{
					if (parameter->pidfile )
					{
						printf( " PID-file set at cmd line, ignoring config\n" );
					}
					else
					{
						parameter->pidfile=calloc(strlen(val)+1,1);
						sprintf(parameter->pidfile,"%s",val);
						printf( " PID-file %s \n",parameter->pidfile );
					}
				}
				else if (strcmp(par,"verbose")==0)
				{
					if (parameter->verbose == 0 || parameter->quiet == 0)
					{
						if (strcmp(val,"yes")==0) parameter->verbose=1; else parameter->verbose=0;
					}
					else printf( " verbose or quiet set at command line, ignoring config\n" );
				}
				else if (strcmp(par,"daemon")==0)
				{
					if (parameter->daemon == 0 && parameter->exec==0)
					{
						if (strcmp(val,"yes")==0) parameter->daemon=1; else parameter->daemon=0;
					}
					else printf( " daemon or exe set at command line, ignoring config\n" );
				}
				
				else if (strcmp(par,"ipv6")==0)
				{
					if ( parameter->ipv6==0 && parameter->ipv4only==0)
					{
						if (strcmp(val,"yes")==0) parameter->ipv6=1; else parameter->ipv6=0;
					}
					else printf( " opv6 or ipv4only set at command line, ignoring config\n" );
				}
	
				else  printf("config command:'%s' not implemented yet \n",par);
			}
		}
	}
	return 0;
}

