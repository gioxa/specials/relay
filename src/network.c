//
//  network.c
//  portsocksmap
//
//  Created by Danny Goossen on 24/4/2020.
//  Copyright © 2020 Danny Goossen. All rights reserved.
//

#include "network.h"

#include "compat.h"

#include "error.h"
//#include "my_mem_debug.h"

#include "socks_conn.h"


void init_network(void)
{
#ifdef __MINGW32__
	WSADATA wsaData;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0)
	{
		printf("WSAStartup failed: %d\n", iResult);
		exit(1);
	}
	;
#endif
	;
	return;
}

void deinit_network(void)
{
#ifdef __MINGW32__
	WSACleanup();
#endif
	;
	return;
}

int socket_nonblocking (sockfd_t sock)
{
	int flags;

	if(sock != INVALID_SOCKET)
	{
#ifdef __MINGW32__
		u_long  argp=1;
		return ioctlsocket(sock, FIONBIO, &argp);
#else
		flags = fcntl (sock, F_GETFL, 0);
		return fcntl (sock, F_SETFL, flags | O_NONBLOCK | O_CLOEXEC | SO_KEEPALIVE);//| O_NDELAY);
#endif
	}
	return -1;

}
int socket_nonblocking_no_keep_alive (sockfd_t sock)
{
	int flags;
	
	if(sock != INVALID_SOCKET)
	{
#ifdef __MINGW32__
		u_long  argp=1;
		return ioctlsocket(sock, FIONBIO, &argp);
#else
		flags = fcntl (sock, F_GETFL, 0);
		return fcntl (sock, F_SETFL, flags | O_NONBLOCK | O_CLOEXEC);//| O_NDELAY);
#endif
	}
	return -1;
	
}

/*
 * Set the socket to blocking -rjkaes
 */
int socket_blocking (sockfd_t sock)
{
	int flags;

	if(sock != INVALID_SOCKET)
	{
#ifdef __MINGW32__
		u_long  argp=0;
		return ioctlsocket(sock, FIONBIO, &argp);
#else

		flags = fcntl (sock, F_GETFL, 0);
		return fcntl (sock, F_SETFL, (flags & ~O_NONBLOCK) | O_CLOEXEC);// | O_NDELAY);
#endif
	}
	return -1;
}






void relaydata(sockfd_t *client_fd, sockfd_t *rem_fd, sockfd_t signalfd , u_int8_t * sess_stop)
{
    struct timeval to;
    to.tv_sec=15;
    to.tv_usec=0;
    struct timeval * sto=&to;
    int             maxfd, nready;
    fd_set          read_masterfds,readfds,writefds,write_masterfds;
    FD_ZERO(&readfds);
    FD_ZERO(&read_masterfds);
    FD_ZERO(&writefds);
    FD_ZERO(&write_masterfds);

    FD_SET(*client_fd, &read_masterfds);
    FD_SET(*rem_fd, &read_masterfds);
    FD_SET(signalfd, &read_masterfds);
    if (*rem_fd > *client_fd)
        maxfd=*rem_fd;
    else
        maxfd=*client_fd;
    if (signalfd > maxfd)
    maxfd=signalfd;

    socket_nonblocking(*rem_fd);
	socket_nonblocking(*client_fd);
    // just to make sure
    u_int16_t socket_flags=0;
#ifndef SO_NOSIGPIPE
    socket_flags=MSG_NOSIGNAL;
#endif
#ifdef SO_NOSIGPIPE
    int set = 1;
    setsockopt(*rem_fd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
    setsockopt(*client_fd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
#endif
    char * data_tx_rem=calloc(0x10000, 1);
    char * data_rx_rem=calloc(0x10000, 1);
    u_int16_t buf_offset_tx_rem=0;
    u_int16_t buf_offset_rx_rem=0;
    ssize_t sendlen=0;
    ssize_t readlen=0;
    ssize_t to_sent_rem=0;
    ssize_t to_sent_cli=0;
    int this_error=0;
    int channel1_ok=0;
    int channel2_ok=0;

    int client_input_closed=0;
    int remote_input_closed=0;

    while(!*sess_stop)
    {
        memcpy(&readfds, &read_masterfds, sizeof(read_masterfds));
        memcpy(&writefds, &write_masterfds, sizeof(write_masterfds));

        nready=select(maxfd+1, &readfds, &writefds, NULL,sto) ;
        sto->tv_sec=300;
        sto->tv_usec=0;
        if (nready==0)
        {
            goto end_this_select;//time_out}
        }
        if (nready==SOCKET_ERROR && errno_sock!=SCK_EINTR)
        {
            goto end_this_select;//stop, error}
        }
        if (*sess_stop )goto end_this_select;

        {
            if(FD_ISSET(signalfd, &readfds)) goto end_this_select;
            do{

                if(to_sent_rem==0 && (FD_ISSET(*client_fd, &readfds) || channel1_ok))
                {
                    nready--;
                    channel1_ok=0;
                    readlen=recv(*client_fd, data_tx_rem ,0x10000,socket_flags );
                    if (readlen == SOCKET_ERROR) this_error=errno_sock;
                    if (readlen == SOCKET_ERROR && (this_error==SCK_EWOULDBLOCK )) {;} // drop through nothing to do more

                    else if (readlen==0 || (readlen==SOCKET_ERROR && this_error!=SCK_EINTR) )
                    {
                        client_input_closed=1;
                        FD_CLR(*client_fd, &read_masterfds);
                        shutdown(*rem_fd, SHUT_WR);
                        sto->tv_sec=5;
                        sto->tv_usec=0;
                    }
                    else if (readlen>0 && readlen!=SOCKET_ERROR)
                    {
                        buf_offset_tx_rem=0;
                        to_sent_rem=readlen;

                        sendlen=send(*rem_fd, (data_tx_rem +buf_offset_tx_rem) , to_sent_rem ,socket_flags);
                        if (sendlen == SOCKET_ERROR) this_error=errno_sock;
                        if (sendlen ==SOCKET_ERROR && (this_error==SCK_EWOULDBLOCK  ))
                        {
                            FD_SET(*rem_fd, &write_masterfds);
                            FD_CLR(*client_fd, &read_masterfds);
                        }
                        else if (sendlen==0 || (sendlen ==SOCKET_ERROR  && this_error!=SCK_EINTR ) )
                        {
                            if (remote_input_closed) goto end_this_select;
                            else
                            {
                                FD_CLR(*rem_fd, &write_masterfds);
                                FD_CLR(*client_fd, &read_masterfds);
                                shutdown(*client_fd, SHUT_RD);
                                sto->tv_sec=5;
                                sto->tv_usec=0;
                                to_sent_rem=0;
                                client_input_closed=1;
                            }
                        }
                        else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen==to_sent_rem)
                        {
                            buf_offset_tx_rem=0;
                            to_sent_rem=0;
                            FD_SET(*client_fd, &read_masterfds);
                            FD_CLR(*rem_fd, &write_masterfds);
                            if (remote_input_closed) goto end_this_select;
                            if(client_input_closed) channel1_ok=0; else channel1_ok=1;
                        }
                        else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen!=to_sent_rem)
                        {
                            buf_offset_tx_rem+=sendlen;
                            to_sent_rem=to_sent_rem-sendlen;
                            FD_SET(*rem_fd, &write_masterfds);
                            FD_CLR(*client_fd, &read_masterfds);
                        }
                    }
                }

                if( to_sent_cli==0 && (FD_ISSET(*rem_fd, &readfds)|| channel2_ok))
                {
                    nready--;
                    channel2_ok=0;
                    readlen=recv(*rem_fd, data_rx_rem ,0x10000,socket_flags );
                    if (readlen ==SOCKET_ERROR ) this_error=errno_sock;
                    if (readlen ==SOCKET_ERROR && (this_error==SCK_EWOULDBLOCK  )) {;} // drop through nothing to do more
                    else if (readlen==0 || (readlen==SOCKET_ERROR && this_error!=SCK_EINTR) )
                    {
                        remote_input_closed=1;
                        FD_CLR(*rem_fd, &read_masterfds);
                        shutdown(*client_fd, SHUT_WR);
                        sto->tv_sec=5;
                        sto->tv_usec=0;
                    }
                    else if (readlen>0 && readlen!=SOCKET_ERROR)
                    {
                        buf_offset_rx_rem=0;
                        to_sent_cli=readlen;
                        sendlen=send(*client_fd, (data_rx_rem +buf_offset_rx_rem) , to_sent_cli , socket_flags);

                        if (sendlen==SOCKET_ERROR) this_error=errno_sock;
                        if (sendlen ==SOCKET_ERROR && (this_error==SCK_EWOULDBLOCK ))
                        {
                            FD_SET(*client_fd, &write_masterfds);
                            FD_CLR(*rem_fd, &read_masterfds);
                        }
                        else if (sendlen==0 || (sendlen == SOCKET_ERROR && this_error!=SCK_EINTR ) )
                        {
                            if (client_input_closed) goto end_this_select;
                            else
                            {
                                FD_CLR(*rem_fd, &read_masterfds);
                                FD_CLR(*client_fd, &write_masterfds);
                                shutdown(*rem_fd, SHUT_RD);
                                sto->tv_sec=5;
                                sto->tv_usec=0;
                                to_sent_rem=0;
                                to_sent_cli=0;
                                remote_input_closed=1;
                            }

                        }
                        else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen==to_sent_cli)
                        {
                            buf_offset_rx_rem=0;
                            to_sent_cli=0;
                            FD_SET(*rem_fd, &read_masterfds);
                            FD_CLR(*client_fd, &write_masterfds);
                            if(client_input_closed) channel2_ok=0;
                            else channel2_ok=1;
                        }
                        else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen!=to_sent_cli)
                        {
                            buf_offset_rx_rem+=sendlen;
                            to_sent_cli=to_sent_cli-sendlen;
                            FD_CLR(*rem_fd, &read_masterfds);
                            FD_SET(*client_fd, &write_masterfds);
                        }
                    }
                }

                if( to_sent_rem > 0 )
                {
                    nready--;
                    channel2_ok=0;
                    sendlen=send(*rem_fd, (data_tx_rem +buf_offset_tx_rem) , to_sent_rem ,socket_flags);
                    if (sendlen==SOCKET_ERROR) this_error=errno_sock;
                    if (sendlen ==SOCKET_ERROR && (this_error==SCK_EWOULDBLOCK )) {;} // just drop true
                    else if (sendlen==0 || (sendlen ==SOCKET_ERROR && errno_sock!=SCK_EINTR) )
                    {
                        if (remote_input_closed) goto end_this_select;
                        else
                        {
                            FD_CLR(*rem_fd, &write_masterfds);
                            FD_CLR(*client_fd, &read_masterfds);
                            shutdown(*client_fd, SHUT_RD);
                            sto->tv_sec=5;
                            sto->tv_usec=0;
                            to_sent_rem=0;
                            client_input_closed=1;
                        }
                    }
                    else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen==to_sent_rem)
                    {
                        buf_offset_tx_rem=0;
                        to_sent_rem=0;
                        FD_SET(*client_fd, &read_masterfds);
                        FD_CLR(*rem_fd, &write_masterfds);

                        if (client_input_closed && remote_input_closed) goto end_this_select;
                        if(client_input_closed)
                        {
                            FD_CLR(*rem_fd, &write_masterfds);
                            shutdown(*rem_fd, SHUT_WR);
                            sto->tv_sec=5;
                            sto->tv_usec=0;

                        }
                        else channel1_ok=1;
                    }
                    else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen!=to_sent_rem)
                    {
                        buf_offset_tx_rem+=sendlen;
                        to_sent_rem=to_sent_rem-sendlen;
                    }
                }

                if( to_sent_cli > 0 )
                {
                    nready--;
                    channel2_ok=0;
                    sendlen=send(*client_fd, (data_rx_rem +buf_offset_rx_rem) , to_sent_cli , socket_flags);
                    if (sendlen==SOCKET_ERROR) this_error=errno_sock;
                    if (sendlen ==SOCKET_ERROR && (this_error==SCK_EWOULDBLOCK  )) {;} // just drop true
                    else if (sendlen==0 || (sendlen == SOCKET_ERROR && errno_sock!=SCK_EINTR) )
                    {
                        if (client_input_closed) goto end_this_select;

                        else
                        {
                            FD_CLR(*rem_fd, &read_masterfds);
                            FD_CLR(*client_fd, &write_masterfds);
                            shutdown(*rem_fd, SHUT_RD);
                            sto->tv_sec=5;
                            sto->tv_usec=0;
                            to_sent_rem=0;
                            to_sent_cli=0;
                            remote_input_closed=1;
                        }
                    }
                    else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen==to_sent_cli)
                    {
                        buf_offset_rx_rem=0;
                        to_sent_cli=0;
                        FD_SET(*rem_fd, &read_masterfds);
                        FD_CLR(*client_fd, &write_masterfds);
                        if (client_input_closed && remote_input_closed) goto end_this_select;
                        if(remote_input_closed)
                        {
                            FD_CLR(*client_fd, &write_masterfds);
                            shutdown(*client_fd, SHUT_WR);
                            sto->tv_sec=5;
                            sto->tv_usec=0;
                        }
                        else channel2_ok=1;
                    }
                    else if (sendlen!=0 && sendlen!=SOCKET_ERROR && sendlen!=to_sent_cli)
                    {
                        buf_offset_rx_rem+=sendlen;
                        to_sent_cli=to_sent_cli-sendlen;
                    }
                }

            } while ((channel1_ok || channel2_ok) && !*sess_stop);
        //done_this_select:
            ;
        }

        ;
    }
     end_this_select:
    shutdown(*rem_fd, SHUT_RDWR);
    close_socket(*rem_fd);
	*rem_fd=INVALID_SOCKET;
	shutdown(*client_fd, SHUT_RDWR);
	close_socket(*client_fd);
	*client_fd=INVALID_SOCKET;
    if (data_tx_rem) free(data_tx_rem);
    if (data_rx_rem) free(data_rx_rem);
    data_rx_rem=NULL;
    data_tx_rem=NULL;
}
