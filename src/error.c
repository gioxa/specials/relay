/*========================================================================
 * error.c  --  Error-handling routines for tblast.
 *
 * just dump all in syslog
 *========================================================================*/

#include "compat.h"
#include "error.h"

#define MAX_ERROR_MESSAGE  512





/*------------------------------------------------------------------------
 * Global variables.
 *------------------------------------------------------------------------*/

char g_error[MAX_ERROR_MESSAGE];

/* set to 1 to log into the syslog */

static int syslog_yn=0;


static  pthread_mutex_t  info_mutex;
static pthread_mutex_t dump_mutex;
void setsyslog()
{
    syslog_yn=1;
}
int getsyslog()
{
    return(syslog_yn);
}
void init_info()
{
    /* create the info mutex */
    pthread_mutex_init(&info_mutex, NULL);
	pthread_mutex_init(&dump_mutex, NULL);

    return;
}

void de_init_info()
{
	pthread_mutex_destroy(&info_mutex);
	pthread_mutex_destroy(&dump_mutex);
}

/*------------------------------------------------------------------------
 * int error_handler(const char *file, int line, const char *message,
 *                   int fatal_yn);
 *
 * Prints an error message (possibly with file and line number
 * information included).  If fatal_yn is true, also aborts the client.
 * The return value is always non-zero to facilitate brevity in code
 * that propagates error conditions upwards.
 *------------------------------------------------------------------------*/

int error_handler(const char *file, int line, const char *message, int fatal_yn)
{
#ifdef DEBUG
     if (syslog_yn) syslog (LOG_NOTICE, "Error condition detected in %s, line %d:", file, line);
    else
        fprintf(stderr, "Error condition detected in %s, line %d:\n", file, line);
    perror("    ");
#endif

    /* print out the message */
    if (syslog_yn)
      syslog (LOG_NOTICE, "%s: %s", (fatal_yn ? "Error" : "Warning"), message);
    else
          fprintf(stderr, "%s: %s\n", (fatal_yn ? "Error" : "Warning"), message);
    if (fatal_yn)
        exit(1);
    return -1;
}

int syslog_sock_error(char * wherefrom , char * msg)
{
	int e=errno_sock;
#ifdef __MINGW32__
	LPVOID lpMsgBuf;
	lpMsgBuf = (LPVOID)"Unknown error";
	e = WSAGetLastError();
	if (FormatMessage(
					  FORMAT_MESSAGE_ALLOCATE_BUFFER |
					  FORMAT_MESSAGE_FROM_SYSTEM |
					  FORMAT_MESSAGE_IGNORE_INSERTS,
					  NULL, e,
					  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					  // Default language
					  (LPTSTR)&lpMsgBuf, 0, NULL))
  {
    if (syslog_yn)
    	syslog(LOG_ERR, "%s : %s, sock_err:##%d: %s",wherefrom,msg, e, lpMsgBuf);
    else
      fprintf(stderr,"%s : %s, sock_err:##%d: %s\n",wherefrom,msg, e, lpMsgBuf);
		LocalFree(lpMsgBuf);
	} else
  {
     if (syslog_yn)
     	syslog( LOG_ERR, "%s : %s, sock_err:#%d",wherefrom,msg, e);
     else
       fprintf(stderr,"%s : %s, sock_err:#%d\n",wherefrom,msg, e);
  }

#else


  if (syslog_yn)
   syslog( LOG_ERR,"%s : %s, sock_err:#%d : %s", wherefrom, msg, e, strerror(e) );
  else
    fprintf(stderr,"%s : %s, sock_err:#%d : %s\n", wherefrom, msg, e, strerror(e) );

#endif
	return e; // return error Num in case we need it later
}

void syslog_sock_error_no(char* wherefrom ,char * msg, int e)
{
#ifdef __MINGW32__
	LPVOID lpMsgBuf;
	lpMsgBuf = (LPVOID)"Unknown error";
	if (FormatMessage(
					  FORMAT_MESSAGE_ALLOCATE_BUFFER |
					  FORMAT_MESSAGE_FROM_SYSTEM |
					  FORMAT_MESSAGE_IGNORE_INSERTS,
					  NULL, e,
					  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					  // Default language
					  (LPTSTR)&lpMsgBuf, 0, NULL))
  {
    if (syslog_yn)
     syslog( LOG_ERR,"%s : %s, sock_err:##%d: %s",wherefrom,msg, e, lpMsgBuf );
    else
      fprintf(stderr,"%s : %s, sock_err:##%d: %s\n",wherefrom,msg, e, lpMsgBuf );
  	LocalFree(lpMsgBuf);
	} else
  {

  if (syslog_yn)
   syslog( LOG_ERR,"%s : %s, sock_err:#%d",wherefrom,msg, e);
  else
    fprintf(stderr,"%s : %s, sock_err:#%d\n",wherefrom,msg, e);
  }
#else
  if (syslog_yn)
   syslog( LOG_ERR,"%s : %s, sock_err:#%d : %s",wherefrom, msg, e, strerror(e) );
  else
    fprintf(stderr,"%s : %s, sock_err:#%d : %s\n",wherefrom, msg, e, strerror(e) );

#endif
	return; // return error Num in case we need it later
}



int hex_dump_msg(u_char*buf,ssize_t size_peek,const char *message, ...)
{
    int i,j;
    char *datapeek=(char*) buf;
    char * li= calloc(size_peek, 6);
    va_list varg;
   // pthread_mutex_lock(&info_mutex);
    if (li)
    {
        va_start (varg, message);
        vsnprintf(li, 6*size_peek , message, varg);

        //sprintf(li,"%s \n",msg);
        for ( i=0;i<size_peek;i++)
        {
            if ((i%32)==0)
            {
                if (i) sprintf(li+strlen(li)," ");
                if (i) for (j=(i-32);(j!=i);j++)
                {
                    if (isprint(datapeek[j]))
                        sprintf(li+strlen(li),"%c",datapeek[j]);
                    else
                        sprintf(li+strlen(li),".");
                }
                if (i!=0) sprintf(li+strlen(li),"\n");
            }
            sprintf(li+strlen(li),"%02x ",(u_int8_t)datapeek[i]);
        }
        if ((i%32))
        {
            if (i) for (j=(i%32);(j!=32);j++) sprintf(li+strlen(li),".. ");
            if (i) for (j=(i-(i%32));(j!=i);j++)
            {
                if (isprint(datapeek[j]))
                    sprintf(li+strlen(li),"%c",datapeek[j]);
                else
                    sprintf(li+strlen(li),".");
            }
            //if (i!=0)sprintf(li+strlen(li),"\n");
        }
        //printf("%02x ",(u_int8_t)datapeek[i]);
        va_end(varg);
        if ((i%32)) sprintf(li+strlen(li),"\n");

        //sprintf(li+strlen(li),""); // TODO find other solution to add \0

        if (syslog_yn)
            syslog (LOG_NOTICE,"%s",li);
        else
            fprintf(stderr, "%s",li);
        //printf("%s",li);

        free(li);
    }
    //pthread_mutex_unlock(&info_mutex);
    return 1;
}

int hex_dump_web(char *datapeek,ssize_t size_peek,char * li )
{
    int i,j;

    li[0]=0;
    for ( i=0;i<size_peek;i++)
    {
        if ((i%32)==0)
        {
            if (i) sprintf(li+strlen(li)," ");
            if (i) for (j=(i-32);(j!=i);j++)
            {
                if (isprint(datapeek[j]))
                    sprintf(li+strlen(li),"%c",datapeek[j]);
                else
                    sprintf(li+strlen(li),".");
            }
            if (i!=0) sprintf(li+strlen(li),"\n");
        }
        sprintf(li+strlen(li),"%02x ",(u_int8_t)datapeek[i]);
    }
    if ((i%32))
    {
        if (i) for (j=(i%32);(j!=32);j++) sprintf(li+strlen(li),".. ");
        if (i) for (j=(i-(i%32));(j!=i);j++)
        {
            if (isprint(datapeek[j]))
                sprintf(li+strlen(li),"%c",datapeek[j]);
            else
                sprintf(li+strlen(li),".");
        }
        //if (i!=0)sprintf(li+strlen(li),"\n");
    }
    //printf("%02x ",(u_int8_t)datapeek[i]);
    if ((i%32)) sprintf(li+strlen(li),"\n");
    //sprintf(li+strlen(li),"");
    return 1;
}


int info( char *message, ...)
{

    pthread_mutex_lock(&info_mutex);

    va_list varg;
    va_start (varg, message);

   if (syslog_yn)
        vsyslog (LOG_NOTICE,message,varg);
    else
       vfprintf(stderr, message,varg);


     va_end(varg);
    pthread_mutex_unlock(&info_mutex);
    return 0;
}

int open_dump (sockfd_t *fd)
{

	*fd=socket(AF_INET,SOCK_DGRAM, 0);
	printf("dumpsocket=%d\n",*fd);

	return 0;

}


void dumpmsg(sockfd_t * fd_dump,u_int32_t dump_ip,u_int16_t dump_pt,char * name,char * host_name,u_int16_t host_port,u_int8_t send, u_char* data_tx,u_int16_t data_size, u_int8_t isssl)
{

	struct	sockaddr_in ai_addr={};
	ai_addr.sin_family =AF_INET;
	ai_addr.sin_port=htons(dump_pt);
	ai_addr.sin_addr.s_addr=dump_ip;
	//ai_addr.sin_len=sizeof(struct sockaddr_in);
	 u_int64_t sslmsg=0;

	u_int16_t sslmsg_len=0;

	//struct sockaddr_in test;

	u_int8_t host_len=strlen(host_name);
	u_int8_t name_len=strlen(name);

	if (*fd_dump !=INVALID_SOCKET)
	{
		struct iovec iov[8];

		iov[0].iov_base=&name_len;
		iov[0].iov_len=1;
		iov[1].iov_base=name;
		iov[1].iov_len=name_len;
		iov[2].iov_base=&host_len;
		iov[2].iov_len=1;
		iov[3].iov_base=host_name;
		iov[3].iov_len=host_len;

		iov[4].iov_base=&send;
		iov[4].iov_len=1;

		if (!isssl)
		{

		iov[5].iov_base=&data_size;
		iov[5].iov_len=2;

		iov[6].iov_base=data_tx;
		iov[6].iov_len=data_size;
		}
		else
		{

			sslmsg= *(u_int64_t *)" SSLDATA";
			sslmsg_len=8;
			((char *)&sslmsg)[0]=data_tx[0];

			iov[5].iov_base=&sslmsg_len;
			iov[5].iov_len=2;

			iov[6].iov_base=&sslmsg;
			iov[6].iov_len=8;

		}

		struct msghdr message;
		bzero(&message, sizeof(message));
		message.msg_name=&ai_addr;
		message.msg_namelen=sizeof(struct sockaddr_in);
		message.msg_iov=iov;
		message.msg_iovlen=7;
		message.msg_control=0;
		message.msg_controllen=0;

		if (sendmsg(*fd_dump,&message,0)==SOCKET_ERROR) {
			syslog_sock_error("dumper   ","Dump send error");
		}


	}







}

/*========================================================================
 * $Log: error.c,v $
 * Revision 1.1.1.1  2006/07/20 09:21:19  jwagnerhki
 * reimport
 *
 * Revision 1.1  2006/07/10 12:27:29  jwagnerhki
 * added to trunk
 *
 */
