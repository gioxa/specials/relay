//
//  error.h
//  ringbuffsort
//
//  Created by Danny Goossen on 27/5/14.
//  Copyright (c) 2014 Danny Goossen. All rights reserved.
//

#ifndef _error_h
#define _error_h

#include "compat.h"

//#define info(message)   info_handler(message)
#define warn(message)   error_handler(__FILE__, __LINE__, (message), 0)
#define error(message)  error_handler(__FILE__, __LINE__, (message), 1)


/* what the hell */

int error_handler(const char *file, int line, const char *message, int fatal_yn);
int info(char *message, ...)
//#if __GNUC__
__attribute__ ((format (printf, 1, 2)))
//#endif
;
int hex_dump_msg(u_char*buf,ssize_t size_peek,const char *message, ...)
#if __GNUC__
__attribute__ ((format (printf, 3, 4)))
#endif
;

int hex_dump_web(char *datapeek,ssize_t size_peek,char * li );
void init_info();
void setsyslog();
int getsyslog();
void dumpmsg(sockfd_t * fd_dump,u_int32_t dump_ip,u_int16_t dump_pt,char * name,char * host_name,u_int16_t host_port,u_int8_t send, u_char* data_tx,u_int16_t data_size, u_int8_t isssl);
void de_init_info();
int open_dump (sockfd_t *fd);
int syslog_sock_error(char * wherefrom,char * msg);
void syslog_sock_error_no(char * wherefrom,char * msg,int e);

#endif
