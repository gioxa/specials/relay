//
//  config.h
//  livestreamer
//
//  Created by Danny Goossen on 29/9/16.
//  Copyright (c) 2016 Danny Goossen. All rights reserved.
//

#ifndef __livestreamer__config__
#define __livestreamer__config__

#include "globals.h"

/*
 *------------------------------------------------------------------------
 * Macro definitions.
 *------------------------------------------------------------------------*/

int process_options(int argc, char **argv, struct parameter_t *parameter);
int readConfigFile(struct parameter_t *parameter);
void set_missing_parms(struct parameter_t * tparameter);
static const char* 		PID_FILE;

#endif /* defined(__livestreamer__config__) */
