
#include "compat.h"



#ifndef HAVE_HTONLL

#ifndef HAVE_BSWAP_64
static u_int64_t bswap_64(u_int64_t in) {
#ifdef ENDIAN_LITTLE
    /* Little endian, flip the bytes around until someone makes a faster/better
     * way to do this. */
    u_int64_t rv = 0;
    int i = 0;
    for(i = 0; i<8; i++) {
        rv = (rv << 8) | (in & 0xff);
        in >>= 8;
    }
    return rv;
#else
    /* big-endian machines don't need byte swapping */
    return in;
#endif
}
#endif

u_int64_t ntohll(u_int64_t val) {
    return bswap_64(val);
}

u_int64_t htonll(u_int64_t val) {
    return bswap_64(val);
}
#endif

/* Global */
//int64_t current_pos_t;

#ifndef HAVE_SENDMSG
ssize_t	sendmsg(sockfd_t fd, const struct msghdr * message, int flags)
{

size_t data_len=0;
int i;
for (i=0;i<message->msg_iovlen;i++)
{
 data_len=data_len+message->msg_iov[i].iov_len;
}
void * data= calloc(1,data_len);
size_t pos=0;
size_t this_len;
for (i=0;i<message->msg_iovlen;i++)
{
	//int thislen=message->msg_iov[i].iov_len;
	this_len=(size_t)message->msg_iov[i].iov_len;
	memcpy(data+pos,message->msg_iov[i].iov_base,this_len);
	pos=pos+this_len;
}
ssize_t return_it=sendto(fd,data,data_len,flags,message->msg_name, message->msg_namelen);
free(data);
return (return_it);
}
#define HAVE_SENDMSG 1
#endif


#ifdef __MINGW32__




LARGE_INTEGER getFILETIMEoffset()
{
    SYSTEMTIME s;
    FILETIME f;
    LARGE_INTEGER t;

    s.wYear = 1970;
    s.wMonth = 1;
    s.wDay = 1;
    s.wHour = 0;
    s.wMinute = 0;
    s.wSecond = 0;
    s.wMilliseconds = 0;
    SystemTimeToFileTime(&s, &f);
    t.QuadPart = f.dwHighDateTime;
    t.QuadPart <<= 32;
    t.QuadPart |= f.dwLowDateTime;
    return (t);
}

int clock_gettime(int X, struct timespec *ts)
{
    LARGE_INTEGER           t;
    FILETIME            f;
    double                  microseconds;
    static LARGE_INTEGER    offset;
    static double           frequencyToMicroseconds;
    static int              initialized = 0;
    static BOOL             usePerformanceCounter = 0;

    if (!initialized) {
        LARGE_INTEGER performanceFrequency;
        initialized = 1;
        usePerformanceCounter = QueryPerformanceFrequency(&performanceFrequency);
        if (usePerformanceCounter) {
            QueryPerformanceCounter(&offset);
            frequencyToMicroseconds = (double)performanceFrequency.QuadPart / 1000000.;
        } else {
            offset = getFILETIMEoffset();
            frequencyToMicroseconds = 10.;
        }
    }
    if (usePerformanceCounter) QueryPerformanceCounter(&t);
    else {
        GetSystemTimeAsFileTime(&f);
        t.QuadPart = f.dwHighDateTime;
        t.QuadPart <<= 32;
        t.QuadPart |= f.dwLowDateTime;
    }

    t.QuadPart -= offset.QuadPart;
    microseconds = (double)t.QuadPart / frequencyToMicroseconds;
    t.QuadPart = microseconds;
    ts->tv_sec = t.QuadPart / 1000000;
    ts->tv_nsec = (t.QuadPart % 1000000)*1000;
    return (0);
}







/*
int gettimeofday(struct timeval * tv,struct timezone *tz )
{ __int64 wintime; GetSystemTimeAsFileTime((FILETIME*)&wintime);
   wintime      -=(long long)116444736000000000;  //1jan1601 to 1jan1970
   tv->tv_sec  =wintime / (long long)10000000;           //seconds
   tv->tv_usec = (wintime % (long long)10000000 )/10;      //micro-seconds
   return 0;
}
*/


#define POW10_3                 1000
#define POW10_4                 10000
#define POW10_6                 1000000
#define POW10_9                 1000000000
#define MAX_SLEEP_IN_MS         4294967294UL

/**
 * Sleep for the specified time.
 * @param  request The desired amount of time to sleep.
 * @param  remain The remain amount of time to sleep.
 * @return If the function succeeds, the return value is 0.
 *         If the function fails, the return value is -1,
 *         with errno set to indicate the error.
 */

 void hnsleep(__int64 hnsec)
 {
     HANDLE timer;
     LARGE_INTEGER ft;

     ft.QuadPart = -(hnsec); // Convert to 100 nanosecond interval, negative value indicates relative time

     timer = CreateWaitableTimer(NULL, TRUE, NULL);
     SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
     WaitForSingleObject(timer, INFINITE);
     CloseHandle(timer);
 }

 void tb_usleep(__int64 hnsec)
 {
     HANDLE timer;
     LARGE_INTEGER ft;

     ft.QuadPart = -(hnsec*10); // Convert to 100 nanosecond interval, negative value indicates relative time

     timer = CreateWaitableTimer(NULL, TRUE, NULL);
     SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
     WaitForSingleObject(timer, INFINITE);
     CloseHandle(timer);
 }


/*
int nanosleep(const struct timespec *request, struct timespec *remain)
{
    unsigned long ms, rc = 0;
    unsigned __int64 u64, want, real;

    union {
        unsigned __int64 ns100;
        FILETIME ft;
    }  _start, _end;

    if (request->tv_sec < 0 || request->tv_nsec < 0 || request->tv_nsec >= POW10_9) {
        errno = SCK_EINTR;
        fprintf(stderr,"Nano sleep error SCK_EINTR\n");
        return -1;
    }
      fprintf(stderr,"Nano get systemtime\n");
    if (remain != NULL) GetSystemTimeAsFileTime(&_start.ft);

    want = u64 = request->tv_sec * POW10_3 + request->tv_nsec / POW10_6;
    while (u64 > 0 && rc == 0) {
        if (u64 >= MAX_SLEEP_IN_MS) ms = MAX_SLEEP_IN_MS;
        else ms = (unsigned long) u64;

        u64 -= ms;
        rc = SleepEx(ms, TRUE);
    }

    if (rc != 0) { // WAIT_IO_COMPLETION (192)
        if (remain != NULL) {
            GetSystemTimeAsFileTime(&_end.ft);
            real = (_end.ns100 - _start.ns100) / POW10_4;

            if (real >= want) u64 = 0;
            else u64 = want - real;

            remain->tv_sec = u64 / POW10_3;
            remain->tv_nsec = (long) (u64 % POW10_3) * POW10_6;
        }

        errno = SCK_EINTR;
        info("nano sleep SCK_EINTR\n ");
        return -1;
    }
      info("nano sleep succeeds\n ");
    return 0;
}
*/
// inet_pton from : http://stackoverflow.com/questions/15370033/how-to-use-inet-pton-with-the-mingw-compiler


int inet_pton(int af, const char *src, void *dst)
{
    switch (af)
    {
    case AF_INET:
        return inet_pton4(src, dst);
    case AF_INET6:
        return inet_pton6(src, dst);
    default:
        return -1;
    }
}

int inet_pton4(const char *src, char *dst)
{
    uint8_t tmp[NS_INADDRSZ], *tp;

    int saw_digit = 0;
    int octets = 0;
    *(tp = tmp) = 0;

    int ch;
    while ((ch = *src++) != '\0')
    {
        if (ch >= '0' && ch <= '9')
        {
            uint32_t n = *tp * 10 + (ch - '0');

            if (saw_digit && *tp == 0)
                return 0;

            if (n > 255)
                return 0;

            *tp = n;
            if (!saw_digit)
            {
                if (++octets > 4)
                    return 0;
                saw_digit = 1;
            }
        }
        else if (ch == '.' && saw_digit)
        {
            if (octets == 4)
                return 0;
            *++tp = 0;
            saw_digit = 0;
        }
        else
            return 0;
    }
    if (octets < 4)
        return 0;

    memcpy(dst, tmp, NS_INADDRSZ);

    return 1;
}

int inet_pton6(const char *src, char *dst)
{
    static const char xdigits[] = "0123456789abcdef";
    uint8_t tmp[NS_IN6ADDRSZ];

    uint8_t *tp = (uint8_t*) memset(tmp, '\0', NS_IN6ADDRSZ);
    uint8_t *endp = tp + NS_IN6ADDRSZ;
    uint8_t *colonp = NULL;

    /* Leading :: requires some special handling. */
    if (*src == ':')
    {
        if (*++src != ':')
            return 0;
    }

    const char *curtok = src;
    int saw_xdigit = 0;
    uint32_t val = 0;
    int ch;
    while ((ch = tolower(*src++)) != '\0')
    {
        const char *pch = strchr(xdigits, ch);
        if (pch != NULL)
        {
            val <<= 4;
            val |= (pch - xdigits);
            if (val > 0xffff)
                return 0;
            saw_xdigit = 1;
            continue;
        }
        if (ch == ':')
        {
            curtok = src;
            if (!saw_xdigit)
            {
                if (colonp)
                    return 0;
                colonp = tp;
                continue;
            }
            else if (*src == '\0')
            {
                return 0;
            }
            if (tp + NS_INT16SZ > endp)
                return 0;
            *tp++ = (uint8_t) (val >> 8) & 0xff;
            *tp++ = (uint8_t) val & 0xff;
            saw_xdigit = 0;
            val = 0;
            continue;
        }
        if (ch == '.' && ((tp + NS_INADDRSZ) <= endp) &&
                inet_pton4(curtok, (char*) tp) > 0)
        {
            tp += NS_INADDRSZ;
            saw_xdigit = 0;
            break; /* '\0' was seen by inet_pton4(). */
        }
        return 0;
    }
    if (saw_xdigit)
    {
        if (tp + NS_INT16SZ > endp)
            return 0;
        *tp++ = (uint8_t) (val >> 8) & 0xff;
        *tp++ = (uint8_t) val & 0xff;
    }
    if (colonp != NULL)
    {
        /*
         * Since some memmove()'s erroneously fail to handle
         * overlapping regions, we'll do the shift by hand.
         */
        const int n = tp - colonp;

        if (tp == endp)
            return 0;

        for (int i = 1; i <= n; i++)
        {
            endp[-i] = colonp[n - i];
            colonp[n - i] = 0;
        }
        tp = endp;
    }
    if (tp != endp)
        return 0;

    memcpy(dst, tmp, NS_IN6ADDRSZ);

    return 1;
}

// TODO: add errors for socket/connect etc
int socketpair(int domain, int type, int protocol, sockfd_t fds[2])
{
    // since this is for mingw only
    domain=AF_INET;
    type=SOCK_DGRAM;
    protocol= IPPROTO_UDP;
    fds[0]=INVALID_SOCKET;
    fds[1]=INVALID_SOCKET;

    struct sockaddr_in inaddr;
    struct sockaddr addr;
    sockfd_t lst=socket(domain, type,protocol);
    if (lst==INVALID_SOCKET)
        {
            //info("no socket for sockpair");
            return SOCKET_ERROR;
        }
    memset(&inaddr, 0, sizeof(inaddr));
    memset(&addr, 0, sizeof(addr));
    inaddr.sin_family = AF_INET;
    inaddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    inaddr.sin_port = 0;
    int yes=1;
#ifdef SO_REUSEADDR
    #ifdef __MINGW32__
             setsockopt(lst,SOL_SOCKET,SO_REUSEADDR, (char *)(&yes), sizeof(yes) );
    #else
             setsockopt(lst,SOL_SOCKET,SO_REUSEADDR, &clear, sizeof(int));
    #endif
#endif
    if(bind(lst,(struct sockaddr *)&inaddr,sizeof(inaddr))== SOCKET_ERROR)
    {
        //info("bind failed\n");
        closesocket(lst);
        return -1;
    }
    int len=sizeof(inaddr);
    getsockname(lst, &addr,&len);
    fds[1]=lst;

    lst=socket(domain, type,protocol);
    if(bind(lst,(struct sockaddr *)&inaddr,sizeof(inaddr))== SOCKET_ERROR)
    {
        //info("bind sock 2 failed\n");
        closesocket(lst);
        closesocket(fds[1]);
        return -1;
    }
    fds[0]=lst;
    connect(fds[0],&addr,len);

    getsockname(fds[0], &addr,&len);
    connect(fds[1],&addr,len);

    return 0;
}
#endif
