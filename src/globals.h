//
//  globals.h
//  relay
//
//  Created by Danny Goossen on 29/9/16 (livestreamer) adapted for relay on July 2022
//  Copyright (c) 2022 Danny Goossen. All rights reserved.
//

#ifndef relay_globals_h
#define relay_globals_h
#include "compat.h"
struct parameter_t
{
	char * host_a;
	u_int16_t port_a;
	char * host_b;
	u_int16_t port_b;
	char * proxy;
	u_int16_t proxyport;
	u_int8_t daemon; // 1 is daemon
	u_int8_t exec;
	char * config_file;
	char * pidfile;
	u_int8_t verbose;
	u_int8_t quiet;
	u_int8_t ipv6;
	u_int8_t ipv4only;
};


#endif
