//
//  main.c
//  portsocksmap
//
//  Created by Danny Goossen on 24/4/2020.
//  Copyright © 2020 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include "globals.h"
#include "config.h"
#include "connect_socks.h"
#include "error.h"
#include "network.h"

char * pidfile=NULL;

#ifndef TEST
	static struct parameter_t * parameters=NULL;
#endif

#ifndef TEST
void init(void)
{
	
#if NEED_STATIC_INIT==1
	init_syslog("");
#endif
	openlog ("streamer", LOG_PID, LOG_USER);
	
	init_network();
return;
}

void de_init(void)
{
  syslog (LOG_NOTICE, " Exit streamer daemon, see u next time!!!");
  deinit_network();
  closelog();
#if NEED_STATIC_INIT==1
  exit_syslog();
#endif
return;
}
#endif

static void signal_handler(sig)
int sig;
{
    int i;
    switch(sig) {
#ifndef __MINGW32__
        case SIGHUP:
            syslog (LOG_NOTICE, " hup Signal Received");
            break;
#else
	case SIGBREAK:
#endif
	case SIGINT:
        
        case SIGTERM:  
	    syslog (LOG_NOTICE, " Signal stop !!!");
#ifndef TEST
            de_init();
#endif
            for (i=getdtablesize();i>=0;--i)
            {
                close_socket(i); /* close all descriptors */
            }
#if HAVE_WORKING_FORK==1
            remove(pidfile);
#endif
            exit(0);
            break;
    }
}




#if ( HAVE_WORKING_FORK==1 || __APPLE__ )

void daemon_it(void)
{
    int i,lfp;
    char str[10];
    pid_t pid;

    if(getppid()==1)
    {
	printf("already a service\n");
        return; /* already a daemon */
    }
    /* Fork off the parent process */
    pid = fork();
    
    /* An error occurred */
    if (pid < 0)
    {
	printf("error on Fork\n");
        exit(EXIT_FAILURE);
    }
    /* Success: Let the parent terminate */
    if (pid > 0)
    {
	printf("success on FORK\n");
        exit(EXIT_SUCCESS);
    } 
    //umask(0x0); /* set newly created file permissions */
    //chdir("/root"); /* change running directory */
    
    /* On success: The child process becomes session leader */
    if (setsid() < 0) 
    {
	printf("problem on setsid\n");
         exit(EXIT_FAILURE);
    }
    printf("opening pid : %s\n",pidfile);
    lfp=open(pidfile, O_RDWR|O_CREAT,0640);
    if (lfp<0)
    {
	printf("Cannot open pid file\n");
        syslog (LOG_NOTICE, "error: streamer cannot open lock file, exit new instance");
        exit(1); /* can not open */
    }
    if (lockf(lfp,F_TLOCK,0)<0)
    {
        printf("streamer already running in background\n");   
        syslog (LOG_NOTICE, "error: streamer already running in background, exit this instance");
        exit(-1); /* can not lock */
    }
    sprintf(str,"%d\n",getpid());
    write(lfp,str,strlen(str)); /* record pid to lockfile */
    {
        printf(" Starting relay in background w pid: %s\n",str);
        printf(" - type 'ps axj | grep relay' to see if it is running or \r\n");
        printf(" - type 'sudo tail -f /var/log/messages | grep streamer => LINUX' to see the system log for relay and \r\n");
#if __ANDROID__==1
        printf(" - type 'cat %s' and  'kill <pid>' to stop the background relay server. Have funn....\r\n\r\n",pidfile);
#else
	printf(" - type (sudo) cat %s xargs kill to stop the background relay. Have funn....\r\n\r\n",pidfile);
#endif
    }
   setlogmask (LOG_UPTO (LOG_DEBUG));	
   openlog ("relay",LOG_CONS | LOG_PID | LOG_NDELAY | LOG_PERROR, LOG_USER);
   syslog (LOG_ALERT, " relay running w pid to : %s",str);
    
    /* Close out the standard file descriptors */
    close_socket(STDIN_FILENO);
    close_socket(STDOUT_FILENO);
    close_socket(STDERR_FILENO);
    
    i=open("/dev/null",O_RDWR); dup(i); dup(i); /* handle standart I/O to dev null*/
   
    
    /* first instance continues */
    signal(SIGCHLD,SIG_IGN); /* ignore child */
    signal(SIGTSTP,SIG_IGN); /* ignore tty signals */
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
}
#endif


#ifdef TEST
int streamer_main(int argc, char ** argv)
{
#else
int main(int argc, char ** argv)
{
	parameters= calloc(sizeof(struct parameter_t),1);
	if (process_options(argc, argv,parameters) != 0)
	{
		fprintf(stderr," *** ERRORin command line options, exit \n");
		return (-1);
	}
	
	if (parameters->config_file)
	{
		if (readConfigFile(parameters))
		{
			return -1;
		}
	}
	
	set_missing_parms(parameters);
#endif
	
#ifndef TEST
	init();
#endif
	
	if (parameters->daemon)
	{
#if ( HAVE_WORKING_FORK==1  || __APPLE__ )
    printf("going to fork\n");
	// put pidfile global, so we can access it from the signal handler
		
	pidfile=parameters->pidfile;
    daemon_it();
    signal(SIGCHLD, SIG_IGN);
#else
printf(" Daemon requested but No Fork in this system\n");
#endif
	}
#ifndef __MINGW32__
    signal(SIGHUP,&signal_handler); /* catch hangup signal */
	
#else
 signal(SIGBREAK,&signal_handler); /* catch kill signal */
	printf("Mingw\n");
#endif

    signal(SIGTERM,&signal_handler); /* catch kill signal */
    signal(SIGINT,&signal_handler); /* catch kill signal */
	
	sockfd_t remfd=0;
	u_int8_t sess_stop=0;
	init_network();

	/* Socket pair allows to communicate to the forks and inform it's over*/
	sockfd_t                  sock_pair_rx[2];
	int status = socketpair(AF_UNIX,SOCK_DGRAM,0,sock_pair_rx);
	if ( status == SOCKET_ERROR ) {
		error(" socketpair(AF_UNIX, SOCK_STREAM, 0)\n");
	}
	
	
	sockfd_t  dest_fd=SOCKET_ERROR;
	sockfd_t  source_fd=SOCKET_ERROR;

	static int counter=0;
	info("started\n");
	for(;;)
	{
		source_fd=connect_host(5,parameters->host_a,parameters->port_a);
		
		if (source_fd!=SOCKET_ERROR)
			dest_fd=connect_host(5,parameters->host_b,parameters->port_b);
		
		if (source_fd!=SOCKET_ERROR && dest_fd!=SOCKET_ERROR)
		{
		info("Relaying Data\n");
		relaydata(&source_fd, &dest_fd, sock_pair_rx[0] , &sess_stop);
		info("Finish relay socket, error\n");
		}
		
		if (source_fd!=SOCKET_ERROR)
			close(source_fd);
		if (source_fd!=SOCKET_ERROR)
			close(source_fd);
		
		source_fd=SOCKET_ERROR;
		dest_fd=SOCKET_ERROR;
		sleep(1);
	}
	deinit_network();

	return 0;
}
