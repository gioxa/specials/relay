# relay

## purpose

transparantly connects 2 tcp ports (bidirectional)

e.g. for dump1090 forwarding.

## Compile

```
autoreconf
./configure
make
sudo make install
```

## usage

```
relay --host_a=192.168.1.207 --porta=30005 --host_b=192.168.1.52 --port_b=30004
```

```
relay --help

Streamer Git Version: 
 
Usage: relay [--verbose] [--v6] [--port=n]
               [--daemon] [--config=/etc/streamer.conf] .... 

version      : Shows the version and usage
help         : Shows the version and usage
verbose      : turns on verbose output mode
quiet        : quiet down output
v4only       : enable only IPV4
v6           : enable IPV6
daemon       : runs tblastd in background as a service
exec         : disable daemon
port_a       : specifies the port of the A host
host_a       : specifies the ipv4 adress of the A host
port_b       : specifies the port of the B host
host_b       : specifies the ipv4 adress of the B host
config       : specifies the config file to be used
               !! use '--config=none' to skip reading a config file all together 
pid          : specifies the pid  file to be used

Defaults: verbose    = 1
          v4only     = 1
          daemon     = 0
          port_a     = 30005
          port_b     = 30004
          host_a     = 127.0.0.1
          host_b     = 127.0.0.1
          socks_port = 1080
          socks_ip   = (null)
          config     = no_config
          pid        = /tmp/relay.pid
```